<?php

declare(strict_types=1);

require_once 'vendor/autoload.php';

$eventBus = require_once 'event_bus.php';

$client = \App\Client\Client::register('1', 'first client');
$clientEvents = $client->getUncommittedEvents();

$events = array_merge(
    $clientEvents,
    [
        new \App\Event\BankAccountConfirmed([
            'clientId' => '1',
            'bankAccountNumber' => '123'
        ]),
        new \App\Event\EmailConfirmed([
            'clientId' => '1',
            'email' => '123'
        ]),
        new \App\Event\EmailConfirmed([
            'clientId' => '1',
            'email' => '123'
        ]),
    ]
);

printf("Start processing events...\n");

foreach ($events as $event) {
    $eventBus->dispatch($event);
}

printf("End processing events...\n");
