<?php

declare(strict_types=1);

require_once 'vendor/autoload.php';

$processes = new \App\Registration\InMemoryRegistrationProcessRepository();
$commandBus = new \Prooph\ServiceBus\CommandBus();
$eventBus = new \Prooph\ServiceBus\EventBus();

$commandRouter = new \Prooph\ServiceBus\Plugin\Router\CommandRouter();

// commands
$commandRouter->route(\App\Command\GrantFullAccessForClient::class)
    ->to(new \App\Command\Handler\GrantFullAccessForClientHandler());
$commandRouter->route(\App\Command\GrantBasicAccessForClient::class)
    ->to(new \App\Command\Handler\GrantBasicAccessForClientHandler());
$commandRouter->route(\App\Command\BeginRegistrationProcessesForClient::class)
    ->to(new \App\Command\Handler\BeginRegistrationProcessesForClient(
        $processes,
        $eventBus
    ));

$commandRouter->attachToMessageBus($commandBus);

// Listeners
$dummyEchoListener = new \App\Event\Handler\DummyEcho();
$grantBasicAccess = new \App\Event\Handler\GrantBasicAccessForClientAfterBasicRegistration(
    $commandBus
);
$grantFullAccess = new \App\Event\Handler\GrantFullAccessForClientAfterExtendedRegistration(
    $commandBus
);
$clientAppliedForRegistration = new \App\Event\Handler\ClientAppliedForRegistrationHandler(
    $commandBus
);

// ProcessManager
$registrationProcess = new \App\Event\Handler\RegistrationProcesses(
    $processes
);

$eventMap = [
    \App\Event\ClientAppliedForRegistration::class => [
        $dummyEchoListener,
        $clientAppliedForRegistration,
    ],
    \App\Event\EmailConfirmed::class => [
        $dummyEchoListener,
        $registrationProcess,
    ],
    \App\Event\BasicRegistrationFinished::class => [
        $dummyEchoListener,
        $grantBasicAccess,
        $registrationProcess,
    ],
    \App\Event\BankAccountConfirmed::class => [
        $dummyEchoListener,
        $registrationProcess,
    ],
    \App\Event\ExtendedRegistrationFinished::class => [
        $dummyEchoListener,
        $grantFullAccess,
    ],
];

$eventRouter = new \Prooph\ServiceBus\Plugin\Router\EventRouter($eventMap);
$eventRouter->attachToMessageBus($eventBus);

return $eventBus;