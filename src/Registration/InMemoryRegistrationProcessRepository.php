<?php

declare(strict_types=1);

namespace App\Registration;

class InMemoryRegistrationProcessRepository implements RegistrationProcessRepository
{
    private $processes = [];

    public function __construct(array $processes = [])
    {
        $this->processes = $processes;
    }

    public function getProcessesForClient(string $id): array
    {
        return array_filter(
            $this->processes,
            function(RegistrationProcessManager $processManager) use ($id) {
                return $processManager->isForClient($id)
                    && !$processManager->isFinalized();
            }
        );
    }

    public function registerProcess(RegistrationProcessManager $process): void
    {
        $this->processes[] = $process;
    }
}
