<?php

declare(strict_types=1);

namespace App\Registration;

use App\Event\BasicRegistrationFinished;

class BasicRegistrationProcess extends RegistrationProcessManager
{
    protected function dispatchEvent(): void
    {
        $this->eventBus->dispatch(
            new BasicRegistrationFinished([
                'clientId' => $this->getClientId(),
            ])
        );
    }
}
