<?php

declare(strict_types=1);

namespace App\Registration;

use App\ProcessManager;
use Prooph\Common\Messaging\DomainEvent;
use Prooph\ServiceBus\EventBus;

abstract class RegistrationProcessManager extends ProcessManager
{
    protected $eventBus;

    private $recordedEvents = [];
    private $requiredEvents;
    private $clientId;
    private $finalized = false;

    public function __construct(
        string $clientId,
        EventBus $eventBus,
        array $requiredEvents
    ) {
        $this->eventBus = $eventBus;
        $this->clientId = $clientId;
        $this->requiredEvents = $requiredEvents;
    }

    public function __invoke(DomainEvent $event)
    {
        if (!in_array(get_class($event), $this->requiredEvents)) {
            return;
        }
        $this->recordedEvents[] = get_class($event);

        if ($this->shouldFinalize()) {
            $this->finalize();
        }
    }

    public function isForClient(string $id): bool
    {
        return $this->clientId === $id;
    }

    public function isFinalized(): bool
    {
        return $this->finalized;
    }

    protected function getClientId(): string
    {
        return $this->clientId;
    }

    protected function finalize(): void
    {
        $this->finalized = true;
        $this->dispatchEvent();
    }

    protected function shouldFinalize(): bool
    {
        sort($this->recordedEvents);
        sort($this->requiredEvents);

        return $this->recordedEvents == $this->requiredEvents
            && !$this->isFinalized();
    }

    protected abstract function dispatchEvent(): void;
}
