<?php

declare(strict_types=1);

namespace App\Registration;

use App\Event\ExtendedRegistrationFinished;

class ExtendedRegistrationProcess extends RegistrationProcessManager
{
    protected function dispatchEvent(): void
    {
        $this->eventBus->dispatch(
            new ExtendedRegistrationFinished([
                'clientId' => $this->getClientId(),
            ])
        );
    }
}
