<?php

declare(strict_types=1);

namespace App\Registration;

interface RegistrationProcessRepository
{
    public function getProcessesForClient(string $id): array;
    public function registerProcess(RegistrationProcessManager $process): void;
}
