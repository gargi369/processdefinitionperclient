<?php

declare(strict_types=1);

namespace App;

abstract class ProcessManager
{
    public abstract function isFinalized(): bool;
    protected abstract function shouldFinalize(): bool;
    protected abstract function finalize(): void;
}
