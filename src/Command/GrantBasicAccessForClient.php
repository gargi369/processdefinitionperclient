<?php

declare(strict_types=1);

namespace App\Command;

use Prooph\Common\Messaging\Command;
use Prooph\Common\Messaging\PayloadTrait;

class GrantBasicAccessForClient extends Command
{
    use PayloadTrait;
}
