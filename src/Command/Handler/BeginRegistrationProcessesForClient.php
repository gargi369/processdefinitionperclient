<?php

declare(strict_types=1);

namespace App\Command\Handler;

use App\Event\BankAccountConfirmed;
use App\Event\BasicRegistrationFinished;
use App\Event\EmailConfirmed;
use App\Registration\BasicRegistrationProcess;
use App\Registration\ExtendedRegistrationProcess;
use App\Registration\RegistrationProcessRepository;
use Prooph\Common\Messaging\Command;
use Prooph\ServiceBus\EventBus;

class BeginRegistrationProcessesForClient
{
    private $registrationProcesses;
    private $eventBus;

    public function __construct(
        RegistrationProcessRepository $registrationProcesses,
        EventBus $eventBus
    ) {
        $this->registrationProcesses = $registrationProcesses;
        $this->eventBus = $eventBus;
    }

    public function __invoke(Command $command)
    {
        $clientId = $command->payload()['clientId'];

        $this->registrationProcesses->registerProcess(
            new BasicRegistrationProcess(
                $clientId,
                $this->eventBus,
                [
                    EmailConfirmed::class,
                ]
            )
        );

        $this->registrationProcesses->registerProcess(
            new ExtendedRegistrationProcess(
                $clientId,
                $this->eventBus,
                [
                    BasicRegistrationFinished::class,
                    BankAccountConfirmed::class,
                ]
            )
        );
    }
}
