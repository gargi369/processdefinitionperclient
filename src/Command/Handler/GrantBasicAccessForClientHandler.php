<?php

declare(strict_types=1);

namespace App\Command\Handler;

use Prooph\Common\Messaging\Command;

class GrantBasicAccessForClientHandler
{
    public function __invoke(Command $command)
    {
        printf(
            "\t\tBasic access granted for: {$command->payload()['clientId']}\n"
        );
    }
}
