<?php

declare(strict_types=1);

namespace App\Command\Handler;

use Prooph\Common\Messaging\Command;

class GrantFullAccessForClientHandler
{
    public function __invoke(Command $command)
    {
        printf(
            "\t\tFull access granted for: {$command->payload()['clientId']}\n"
        );
    }
}
