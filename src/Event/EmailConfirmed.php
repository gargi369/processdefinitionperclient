<?php

declare(strict_types=1);

namespace App\Event;

use Prooph\Common\Messaging\DomainEvent;
use Prooph\Common\Messaging\PayloadTrait;

class EmailConfirmed extends DomainEvent
{
    use PayloadTrait;
}
