<?php

declare(strict_types=1);

namespace App\Event\Handler;

use App\Registration\RegistrationProcessRepository;
use Prooph\Common\Messaging\DomainEvent;

class RegistrationProcesses
{
    private $registrationProcesses;

    public function __construct(
        RegistrationProcessRepository $registrationProcesses
    ) {
        $this->registrationProcesses = $registrationProcesses;
    }

    public function __invoke(DomainEvent $event)
    {
        $clientId = $event->payload()['clientId'];
        $processes = $this->registrationProcesses->getProcessesForClient(
            $clientId
        );
        printf("Processes in progress: %d\n", count($processes));

        foreach ($processes as $process) {
            $process($event);
        }
    }
}
