<?php

declare(strict_types=1);

namespace App\Event\Handler;

use App\Command\GrantBasicAccessForClient;
use App\Event\BasicRegistrationFinished;
use Prooph\ServiceBus\CommandBus;

class GrantBasicAccessForClientAfterBasicRegistration
{
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(BasicRegistrationFinished $event)
    {
        $this->commandBus->dispatch(
            new GrantBasicAccessForClient(
                [
                    'clientId' => $event->payload()['clientId'],
                ]
            )
        );
    }
}
