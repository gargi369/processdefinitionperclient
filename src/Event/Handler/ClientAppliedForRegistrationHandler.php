<?php

declare(strict_types=1);

namespace App\Event\Handler;

use App\Command\BeginRegistrationProcessesForClient;
use App\Event\ClientAppliedForRegistration;
use Prooph\ServiceBus\CommandBus;

class ClientAppliedForRegistrationHandler
{
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(ClientAppliedForRegistration $event)
    {
        $this->commandBus->dispatch(
            new BeginRegistrationProcessesForClient(
                [
                    'clientId' => $event->payload()['clientId'],
                ]
            )
        );
    }
}
