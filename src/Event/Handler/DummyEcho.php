<?php

declare(strict_types=1);

namespace App\Event\Handler;

use Prooph\Common\Messaging\DomainEvent;

class DummyEcho
{
    public function __invoke(DomainEvent $event)
    {
        printf("\t - " . get_class($event) . "\n");
    }
}
