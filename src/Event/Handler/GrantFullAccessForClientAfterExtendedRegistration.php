<?php

declare(strict_types=1);

namespace App\Event\Handler;

use App\Command\GrantFullAccessForClient;
use App\Event\ExtendedRegistrationFinished;
use Prooph\ServiceBus\CommandBus;

class GrantFullAccessForClientAfterExtendedRegistration
{
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    public function __invoke(ExtendedRegistrationFinished $event)
    {
        $this->commandBus->dispatch(
            new GrantFullAccessForClient(
                [
                    'clientId' => $event->payload()['clientId'],
                ]
            )
        );
    }
}
