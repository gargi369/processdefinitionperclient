<?php

declare(strict_types=1);

namespace App\Client;

use App\Event\ClientAppliedForRegistration;

class Client
{
    private $id;
    private $name;
    private $events = [];

    protected function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public static function register(string $id, string $name)
    {
        $instance = new self($id, $name);
        $instance->events[] = new ClientAppliedForRegistration([
            'clientId' => $id,
            'name' => $name,
        ]);

        return $instance;
    }

    public function getUncommittedEvents(): array
    {
        return $this->events;
    }

    public function dispatched(): void
    {
        $this->events = [];
    }
}
